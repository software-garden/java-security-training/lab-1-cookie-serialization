include Makefile.d/defaults.mk

all: ## (Default) Clean and test
all: test dist

dist:
	mkdir --parents $@
	echo "<p>All is well</p>" > $@/index.html

test: ## Test if program is working correctly
test: test-copernicus
test: test-galileo
test: test-attacker
	########################################
	#                                      #
	# EVERYTHING IS GOOD! CONGRATULATIONS! #
	#                                      #
	########################################
.PHONY: test

test-copernicus: ## Test if Copernicus can authenticate
test-copernicus: copernicus.response
	#
	# ASSERTIONS FOR COPERNICUS:
	#
	# Login was successful:
	#
	( grep 'Welcome back, copernicus!' copernicus.response )
	#
	# But copernicus is not an administrator:
	#
	(! grep 'You are an administrator' copernicus.response )
	#
.PHONY: test-copernicus

test-galileo: ## Test if Galileo can authenticate
test-galileo: galileo.response
	#
	# ASSERTIONS FOR GALILEO:
	#
	# Login was successful:
	#
	( grep 'Welcome back, galileo!' galileo.response )
	#
	# And Galileo is an administrator:
	#
	( grep 'You are an administrator' galileo.response )
	#
.PHONY: test-galileo

test-attacker: ## Test if program is vulnerable to serialization tampering
test-attacker: attacker.response
	# ASSERTIONS FOR ATTACKER
	#
	# Login was not successful:
	#
	( ! grep 'Welcome back' attacker.response )
	#
	# The error message indicates that signature is invalid
	#
	( grep "Signature verification has failed." attacker.response )
	#
.PHONY: test-attacker


install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

attacker.cookie: attacker.ser
	base64 --wrap=0 attacker.ser \
	> attacker.cookie

%.response: %.cookie Session.class
	- java Session $$(cat $<) \
	2>&1 \
	| tee $@

attacker.ser: copernicus.ser
# NOTE: Typically you would do this part with a hex editor. Here we emulate the following manual steps:
# 1. Open copernicus.ser in a hex editor (like tweak or hexedit)
# 2. Search for ASCII string "regular"
# 3. Right before it ther is a byte with a value 0x07 (length of "regular")
# 4. Replace this byte with the value 0x0D (i.e. 13 - the length of "administrator")
# 5. In ASCII replace "regular" with "administrator"

	# calculate hexadecimal values for strings
	regular=$$(echo -n "regular" | xxd -postscript)
	administrator=$$(echo -n "administrator" | xxd -postscript)

	xxd -postscript copernicus.ser \
	| tr --delete '\n' \
	| sed s/07$${regular}/0d$${administrator}/g \
	| xxd -revert -postscript \
	> attacker.ser

copernicus.ser: copernicus.cookie
	base64 --decode copernicus.cookie \
	> copernicus.ser

copernicus.cookie: Session.class
	java Session copernicus Poland \
	| tail --lines=2 \
	| head --lines=1 \
	> copernicus.cookie

galileo.ser: galileo.cookie
	base64 --decode galileo.cookie \
	> galileo.ser

galileo.cookie: Session.class
	java Session galileo Italy \
	| tail --lines=2 \
	| head --lines=1 \
	> galileo.cookie

Session.class: Session.java User.java
Session.class:
	javac Session.java

### DEVELOPMENT

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)
