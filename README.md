# Lab 1. Session cookie

This repository demonstrates the attack on Java serialization. It contains a simplified application that allows users to log in with their username and password. There are two users:

- Galileo with username `galileo` and password `Italy`. He is an administrator.
- Copernicus with username `copernicus` and password `Poland`. He is a regular user.

In exchange for credentials the application issues a session cookie that can be used to interact with it. To a typical user the value of the cookie is meaningless. It could be just a session id.

But Copernicus is sneaky and wants to elevate his privileges to be an administrator too. He figured out that the cookie is a base64 encoded object serialized by Java. Further more, the username and role (`administrator` or `regular`) is contained in the serialized data. So if he could modify the data and replace `regular` with `administrator`, then he would play a prank on Galileo.

## The attack

The exact steps of the attack are codified in the [Makefile](./Makefile). Here is how it's done:

1.  Copernicus logs in using his username and password

    ``` 
    $ java Session copernicus Poland

    Hello copernicus, here is your SESSION cookie:
    rO0ABXNyAARVc2VyvbAAnCNsiNICAAJMAARyb2xldAASTGphdmEvbGFuZy9TdHJpbmc7TAAIdXNlcm5hbWVxAH4AAXhwdAAHcmVndWxhcnQACmNvcGVybmljdXMKoACcW3MPSldISRb/POfnCd0Hu5nYOvKF5djTeb+uIVUuhPrgAIa86Vw9rnAYv81Lh98iC3tX9TUsBB9lV6VZIMzI0o1G4FUbx3nNhVtak7zXKdPu/3/6fgxOFQtK/sYqthByny+bHntKI/5Nk+UsM0AVW1yYHyxkk5AiWBIaJw==

    ```
    
2.  He takes the cookie, saves it as `copernicus.cookie` and decodes it into the binary data

    ```
    $ base64 --decode copernicus.cookie > copernicus.ser
    ```
    
3.  Using a hex editor like `tweak` or `hexedit` he modifies the data in the following way:
    
    1. He finds the string `regular`
    2. Immediately before it is a byte that indicates the length of this string. Its hex value is `07`
    3. He wants the string to say `administrator`. Since the length of this string is longer (13 characters) he needs to change the byte to `0D` (hex representation for number 13).
    4. Next he replaces the string `regular` to `administrator`.
    5. He saves the modified file as `attacker.ser`

4.  The application expects the cookie to be base64 encoded.

    ```
    $ base64 attacker.ser > attacker.cookie
    ```
    
5.  Now he can pass the cookie to the application
    
    ```
    $ java Session $(cat attacker.cookie)


    Welcome back, copernicus!
    You are an administrator, be careful!

    ```
    
![Nicolaus Copernicus](./copernicus.jpg "Nicolaus Copernicus right after he pwnd Galileo Galilei")

## Your job

Copernicus may only want to play a prank, but someone else can do something really malicious. Maybe modify the astronomical data to suggest that the Earth is flat? That would be terribly bad. You need to prevent it.

Notice that the CI is failing. That's because there are already tests in place to make sure this kind of thing can't happen.

Fork the repository and make the CI pass. Try to avoid modifying anything else than the Java code. Notice that the tests expect specific exception to be thrown if the attack is detected.

This is meant to be a group exercise. Together try to find a most elegant solution.

## Extras

- Discuss when data can be considered trusted or untrusted. Why is a cookie untrusted? Find other examples.

- What other ways of preventing this attack are there? In what circumstances would one be preferred over another?
    
## Credit

The code and attack technique is adapted from <https://github.com/kojenov/serial/tree/master/2.%20cookie>. Most of the credit goes to Alexei Kojenov. We highly recommend watching his presentation about serialization attacks: <https://www.youtube.com/watch?v=t-zVC-CxYjw>
